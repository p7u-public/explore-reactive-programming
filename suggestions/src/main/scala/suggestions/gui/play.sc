package suggestions.gui

import rx.lang.scala.Observable
import rx.lang.scala.Observer
import scala.concurrent.duration._
import rx.lang.scala.Subscription
import rx.lang.scala.subjects._
import rx.lang.scala.Observer
import rx.lang.scala.subscriptions.Subscription

object play {
	
	val numbers = Observable((1 to 10))       //> numbers  : rx.lang.scala.Observable[Int] = rx.lang.scala.Observable$$anon$9@
                                                  //| 7546d205


  val test = numbers map(v => v) subscribe ({ println(_) },
  								 													{ t => println("Error encountered") },
  								 													{ () => println("Sequence complete") })
                                                  //> 1
                                                  //| 2
                                                  //| 3
                                                  //| 4
                                                  //| 5
                                                  //| 6
                                                  //| 7
                                                  //| 8
                                                  //| 9
                                                  //| 10
                                                  //| Sequence complete
                                                  //| test  : rx.lang.scala.Subscription = rx.lang.scala.subscriptions.Subscriptio
                                                  //| n$$anon$1@5b980c78
                                                  
	val f: Observer[Int] => Subscription = { o => numbers subscribe ({ println(_) },
  								 													 { t => println("Error encountered") },
  								 													 { () => println("Sequence complete") }) }
                                                  //> f  : rx.lang.scala.Observer[Int] => rx.lang.scala.Subscription = <function1>
                                                  //| 
  								 													 
 	
  
  def myf: Observable[String] = { val s = ReplaySubject[String]; s.onNext("test"); Observable(s.subscribe(_)) }
                                                  //> myf: => rx.lang.scala.Observable[String]
  ///
  /*
  val strings = Observable("a", "b", "c")
  val sub = ReplaySubject[String]
  
  val s1 = sub subscribe ({ println(_) })
  val s2 = sub subscribe ({ println(_) })
  
  //val fin = sub ++ strings
  sub.onNext("s")
  sub.onNext("t")
  sub.onNext("u")
  */
  
  //fin map { v => v }
  
  //zz
  val observed = collection.mutable.Buffer[String]()
                                                  //> observed  : scala.collection.mutable.Buffer[String] = ArrayBuffer()
  val sub = myf subscribe { observed += _ }       //> sub  : rx.lang.scala.Subscription = rx.lang.scala.subscriptions.Subscriptio
                                                  //| n$$anon$1@54efd40d
  val s = ReplaySubject[String]                   //> s  : rx.lang.scala.subjects.ReplaySubject[String] = rx.lang.scala.subjects.
                                                  //| ReplaySubject@4d4acd0b
  
  observed                                        //> res0: scala.collection.mutable.Buffer[String] = ArrayBuffer(test)
  //myf.
}